use std::io;

fn main() {
    // Buffer for user input
    let mut text = String::new();

    // Prompt user for input
    println!("Text:");
    io::stdin()
	.read_line(&mut text)
	.expect("Failed to read input!");

    // Count occurence of letters, words, and sentences in users input text
    let letters = text.trim()
	.chars()
	.filter(|c| c.is_alphabetic())
	.count() as f64;
    let words = 1.0 + text.trim()
	.chars()
        .filter(|c| c.is_whitespace())
        .count() as f64;
    let sentences = text.trim()
	.chars()
	.filter(|&c| c == '.' || c == '?' || c == '!')
	.count() as f64;

    // Calculate percentage of letters per words, and sentences per words
    let l = 100.0 * letters / words;
    let s = 100.0 * sentences / words;
    // Calculate "Coleman-Liau index"
    let cl_idx = (0.0588 * l - 0.296 * s - 15.8).round() as u8;

    // Display "Coleman-Liau index of text
    if cl_idx < 1 {
	println!("Before Grade 1");
    } else if cl_idx > 16 {
	println!("Grade 16+");
    } else {
	println!("Grade {}", cl_idx);
    }
}
