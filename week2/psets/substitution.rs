use std::io;
use std::collections::HashMap;
use std::env;
use std::process;

const ALPHA_VEC: [char; 26] = ['a', 'b', 'c', 'd', 'e', 'f',
			       'g', 'h', 'i', 'j', 'k',
			       'l', 'm', 'n', 'o', 'p',
			       'q', 'r', 's', 't', 'u',
			       'v', 'w', 'x', 'y', 'z'];

fn main() {
    let argv: Vec<String> = env::args().collect();
    let argc = argv.len();

    // Exit program with status code 1 if two arguments aren't given
    if argc != 2 {
	println!("Wrong number of arguments given!");
	process::exit(1);
    }

    // Check validit of key given
    let key = argv[1].to_lowercase();
    if !valid_key(&key) {
	process::exit(1);
    }

    // Map latin alphabet to cipher key
    let mut ciphermap: HashMap<char, char> = HashMap::new();
    for (&k, v) in ALPHA_VEC.iter().zip(key.chars()) {
	ciphermap.insert(k, v);
    }

    // Initialize buffer for user input
    let mut plaintext = String::new();

    // Prompt user for input
    println!("plaintext:");
    io::stdin()
        .read_line(&mut plaintext)
        .expect("Failed to read input!");

    // Substitute plain text with cipher text
    let ciphertext: String = plaintext.trim()
        .chars()
        .map(|c| {
	    if c.is_ascii_lowercase() {
		*ciphermap.get(&c).unwrap()
	    } else if c.is_ascii_uppercase() {
		(*ciphermap.get(&c.to_ascii_lowercase()).unwrap()).to_ascii_uppercase()
	    } else {
		c
	    }
	}).collect();
    // Display cipher text
    println!("ciphertext:\n{}", ciphertext);
}

/// Given a key, this function determines if it is valid
/// to uses as a key for a substituion cipher
fn valid_key(key: &str) -> bool {
    let num_chars = key.len();
    let num_iter = num_chars - 2;

    // The key is invalid if it's not 26 characters long
    if num_chars != 26 {
	println!("The key has an insufficient number of characters!");
	return false;
    }

    // The key is invalid if it contains anything other than latin letters
    if !key.chars().all(|c| c.is_ascii_alphabetic()) {
	println!("The key contains non-alphabetic characters!");
	return false;
    }

    // The key is invalid if it has repeated characters
    for (i, c0) in key.chars().enumerate() {
	for c1 in key.chars().skip(i + 1) {
	    if c0 == c1 {
		println!("The key contains repeated characters!");
		return false;
	    }
	}
	
	if i == num_iter {
	    break;
	}
    }

    // The key is valid if none of the previous conditions triggered
    true
}
