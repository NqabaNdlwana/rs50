use std::io;
use std::env;
use std::process;
use std::convert::TryInto;

fn main() {
    let argv: Vec<String> = env::args().collect();
    let argc = argv.len();

    if argc != 2 {
	println!("Usage: ./caesar [KEY]");
	process::exit(1);
    }

    let key: u8 = match argv[1].parse::<u32>() {
	Ok(n) if n <= (u32::MAX - 26) => (n % 26).try_into().expect("Conversion failed!"),
	_ => {
	    println!("Usage: ./caesar [KEY]");
	    process::exit(1);
	},
    };
    
    let mut plaintext = String::new();

    println!("plaintext:");
    io::stdin()
        .read_line(&mut plaintext)
        .expect("Failed to read input!");

    if !plaintext.is_ascii() {
	println!("This program only supports ASCII characters!");
	process::exit(1);
    }

    let ciphertext: String = plaintext.trim().chars().map(|c| {
	if c.is_ascii_uppercase() {
	    if key + c as u8 > b'Z' {
		(key + c as u8 - 26) as char
	    } else {
		(key + c as u8) as char
	    }
	} else if c.is_ascii_lowercase() {
	    if key + c as u8 > b'z' {
		(key + c as u8 - 26) as char
	    } else {
		(key + c as u8) as char
	    }
	} else {
	    c
	}
    }).collect();

    println!("ciphertext: {ciphertext}");

}
