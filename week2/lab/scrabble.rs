use std::io;
use std::collections::HashMap;

fn main() {
    let points: HashMap<char, u16> = HashMap::from(
	[
	    ('a', 1),
	    ('b', 3),
	    ('c', 3),
	    ('d', 2),
	    ('e', 1),
	    ('f', 4),
	    ('g', 2),
	    ('h', 4),
	    ('i', 1),
	    ('j', 8),
	    ('k', 5),
	    ('l', 1),
	    ('m', 3),
	    ('n', 1),
	    ('o', 1),
	    ('p', 3),
	    ('q', 10),
	    ('r', 1),
	    ('s', 1),
	    ('t', 1),
	    ('u', 1),
	    ('v', 4),
	    ('w', 4),
	    ('x', 8),
	    ('y', 4),
	    ('z', 10)
	]
    );

    let mut word1 = String::new();
    let mut word2 = String::new();

    println!("Player 1:");
    io::stdin()
	.read_line(&mut word1)
	.expect("Failed to read 'Player 1s' input!");
    println!("Player 2:");
    io::stdin()
        .read_line(&mut word2)
        .expect("Failed to read 'Player 2s' input!");

    let score1 = compute_score(word1.to_lowercase(), &points);
    let score2 = compute_score(word2.to_lowercase(), &points);

    if score1 > score2 {
	println!("Player 1 wins!");
    } else if score1 < score2 {
	println!("Player 2 wins!");
    } else {
	println!("Tie!");
    }
}

fn compute_score(word: String, points: &HashMap<char, u16>) -> u16 {
    word.chars()
	.map(|c| points.get(&c).unwrap_or(&0))
	.sum()
}
