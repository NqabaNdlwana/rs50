use std::io;

fn main() {
    let height = get_height();

    for num_bricks in 1..=height {
	print_pyramid(height - num_bricks, num_bricks);
    }
}

/// A function to get user input which will be the
/// height of the structure from mario to be printed
fn get_height() -> u8 {
    // Initialize buffer for user input
    let mut height: String = String::new();

    // Continuously prompt the user until a valid input is given
    loop {
	println!("Height:");
	io::stdin()
	    .read_line(&mut height)
	    .expect("Failed to read input!");

	// Ensure values between 1 and 8 are entered
	match height.trim().parse::<u8>() {
	    Ok(n) if n > 0 && n < 9 => break n,
	    Ok(_) | Err(_) => {
		height.clear();
		println!("Please enter a number from 0 till 8.");
	    },
	}
    }
}

/// A function that takes a number and a character as input
/// and prints a line of that number of characters
fn print_pyramid(padding: u8, num_bricks: u8) {
    let spaces: String = (0..padding).map(|_| ' ').collect();
    let bricks: String = (0..num_bricks).map(|_| '#').collect();
    println!("{0}{1}  {1}", spaces, bricks);
}
