use std::io;

fn main() {
    let mut dollars = String::new();

    let change = loop {
	println!("Change owed:");
	io::stdin()
	    .read_line(&mut dollars)
	    .expect("Failed to read input!");

	match dollars.trim().parse::<f32>() {
	    Ok(n) if n >= 0.0 => break n,
	    _ => {
		dollars.clear();
		continue;
	    },
	}
    };

    let mut cents = (100.0 * change).round() as u32;

    let quarters = cents / 25;
    cents -= 25 * quarters;
    let dimes = cents / 10;
    cents -= 10 * dimes;
    let nickels = cents / 5;
    cents -= 5 * nickels;
    let coins = quarters + dimes + nickels + cents;

    println!("{coins}");
}
