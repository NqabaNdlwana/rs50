use std::io;

fn main() {
    // Initialize buffer for user input
    let mut card_number = String::new();

    // Prompt user for input
    println!("Card number:");
    io::stdin()
        .read_line(&mut card_number)
	.expect("Failed to read input!");

    // Convert `String` to `Vec<u32>`
    let card_number: Vec<u32> = card_number
	.trim()
	.chars()
	.map(|c| c.to_digit(10).unwrap())
	.collect();

    // Check if credit card is valid
    if let Some(company) = card_company(&card_number) {
	if luhn_algo(&card_number) {
	    println!("{}", company);
	} else {
	    println!("INVALID");
	}
    } else {
	println!("INVALID");
    }
}

/// This function determine the credit card company based
/// on the credit card number provided as input
fn card_company(num: &Vec<u32>) -> Option<&str> {
    // The details required include the number of digits,
    // as well as the first one or two digits
    let card_details = (num.len(), num[0], num[1]);

    match card_details {
	// Ameican Express cards start with "34" or "37",
	// they also are 15 digits long
	(15, 3, n) if
	    n == 4 ||
	    n == 7 => Some("AMEX"),
	// MasterCard cards strat with "51", "52", "53", "54", or "55",
	// and they are 16 digits long
	(16, 5, n) if
	    n == 1 ||
	    n == 2 ||
	    n == 3 ||
	    n == 4 ||
	    n == 5 => Some("MASTERCARD"),
	// Visa cards start with "4", and
	// they're 13 or 16 digits long
	(m, 4, _) if
	    m == 13 ||
	    m == 16 => Some("VISA"),
	// Account case where no conditions met
	_ => None,
    }
}

/// Luhn's algorithm uses a checksum to determine the
// validity of a credit card given the card number
fn luhn_algo(num: &Vec<u32>) -> bool {
    let len = num.len();
    
    // Sum the digits of every second digit times two.
    // Starting with the second last digit going left.
    let step1: u32 = num[0..(len - 1)]
	.iter()
	.rev()
	.step_by(2)
	.map(|n| if n * 2 > 9 {
	    1 + (n * 2) % 10
	} else {
	    n * 2
	}).sum();
    // Sum all the other digits excluded from the previous step
    let step2: u32 = num
	.iter()
	.rev()
	.step_by(2)
	.sum();

    // The credit card number is valid if
    // the last digit of the sum of the two sums is zero
    (step1 + step2) % 10 == 0
}
