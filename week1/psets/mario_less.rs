use std::io;

fn main() {
    let mut h = String::new();

    let height = loop {
	println!("Height:");
	io::stdin()
	    .read_line(&mut h)
	    .expect("Failed to read input!");

	match h.trim().parse::<u8>() {
	    Ok(n) if n > 0 && n < 9 => break n,
	    Ok(_) | Err(_) => {
		h.clear();
		continue;
	    },
	}
    };

    for num_bricks in 1..=height {
	let spaces: String = (0..(height - num_bricks)).map(|_| ' ').collect();
	let bricks: String = (0..num_bricks).map(|_| '#').collect();
	println!("{spaces}{bricks}");
    }
}
