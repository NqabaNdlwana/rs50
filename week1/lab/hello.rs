use std::io;

fn main() {
    // Initialize buffer for input
    let mut name = String::new();

    // Prompt user for input
    println!("What's your name?");
    io::stdin()
        .read_line(&mut name)
        .expect("Failed to read input!");

    // Print greeting with user input
    println!("Hello, {}", name);
}
